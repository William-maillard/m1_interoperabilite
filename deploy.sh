#!/usr/bin/bash
dockeruser="williammaillard"
dockerrepo="alpestransportui"

# buils a container for the application
# mvn clean install jib:dockerBuild

# tag the image to send to docker hub (assume you are login with docker login)
docker tag "$dockerrepo" "$dockeruser/$dockerrepo"

docker push "$dockeruser/$dockerrepo"