FROM python:3.10-alpine3.18

WORKDIR /usr/src/app

COPY ./interface/ ./
RUN pip install --no-cache-dir -r requirements.txt

CMD ["gunicorn", "-w", "4", "-b", "0.0.0.0:80", "app:app"]
